LDLIBS=-lpcap 

all: deauth-attack

deauth-attack: deauth-attack.c deauth-attack.h
	$(LINK.cc) $^ $(LDLIBS) -o $@ -fpermissive

clean:
	rm -f deauth-attack *.o
