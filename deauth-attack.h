#include <stdint.h>

#define SIZE_ETHERNET 14

/* IEEE 802.11 Radiotap header */
struct deauth_radiotap_header {
    uint8_t  it_version; 
    uint8_t  it_pad;
    uint16_t it_len;
    uint32_t it_present;
    uint8_t  it_rate;
    uint8_t dummy;
    uint16_t it_tx_flags;
} __attribute__((packed));

struct auth_radiotap_header {
    uint8_t  it_version; 
    uint8_t  it_pad;
    uint16_t it_len;
    uint64_t it_present;
    uint8_t  it_flags;
    uint8_t  it_rate;
    uint16_t it_freq;
    uint16_t it_ch_flags;
    uint8_t it_ant_signal;
    uint8_t dummy;
    uint16_t it_rx_flags;
    uint8_t it_ant_signal2;
    uint8_t it_ant;
} __attribute__((packed));

struct deauth_header {
    uint16_t frame_control;
    uint16_t duration;
    uint8_t  addr1[6];
    uint8_t  addr2[6];
    uint8_t  addr3[6];
    uint16_t sequence_control;
    uint16_t reason_code;
} __attribute__((packed));

struct auth_header {
    uint16_t frame_control;
    uint16_t duration;
    uint8_t  addr1[6];
    uint8_t  addr2[6];
    uint8_t  addr3[6];
    uint16_t sequence_control;
    //(Fixed parameter)
    uint16_t algo;
    uint16_t seq;
    uint16_t status_code;
} __attribute__((packed));